
ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents
$ git config --global --list   #checked if binded to email and username gitlab
user.email=bihag.rudolf@gmail.com
user.name=bihag.rudolf

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents
$ pwd    #check what/where is the working directory
/c/Users/ASUS/Documents

[ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents
$ mkdir practice && cd practice frontend && cd frontend
bash: cd: too many arguments

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents
$ mkdir practice && cd practice
mkdir: cannot create directory �practice�: File exists] trial and error

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents
$ cd practice   #change directory/go to "practice"

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice
$ mkdir frontend  #create frontend folder

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice
$ mkdir p02 discussion  #created p02 and discussion folder, deleted both folders afterwards

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice
$ cd frontent
bash: cd: frontent: No such file or directory

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice
$ cd frontend   #go to "frontend"

[ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend
$ mkdir p02 && cd p02 mkdir discussion
bash: cd: too many arguments

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend
$ mkdir p02 && cd p02
mkdir: cannot create directory �p02�: File exists]   #trial and error

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend
$ mkdir p02 && cd p02   #create p02 folder inside frontend and go to p02 folder

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend/p02
$ mkdir discussion activity   #create 2 folders "discussion" "activity" in "p02"

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend/p02
$ cd ..   #move up/out from folder p02>>frontend

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend
$ git init  #create ".git folder(hidden)" inside frontend folder and make it as (master)
Initialized empty Git repository in C:/Users/ASUS/Documents/practice/frontend/.git/

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git status  #check if you have commits/changes
On branch master

No commits yet

nothing to commit (create/copy files and use "git add" to track)

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ cd p02  #go to p02

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend/p02 (master)
$ cd discussion   #go to discussion

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend/p02/discussion (master)
$ touch discussion.txt  #create discussion text file deleted afterwards

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend/p02/discussion (master)
$ touch discussionpractice.txt  #create discussionpractice text file

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend/p02/discussion (master)
$ cd ..   #move up/out of folder discussion>>p02

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend/p02 (master)
$ cd ..   #move up/out of folder p02>>frontend

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git status  #notice that there is an untracked file
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        p02/

nothing added to commit but untracked files present (use "git add" to track)

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git add .   #to add all new files created in frontend folder

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git status  #to check if you can now create 1 commit from the changes you have done
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   p02/discussion/discussionpractice.txt


ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git commit -m "initial practice commit"   #labeling the first commit/log
[master (root-commit) 188c9de] initial practice commit
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 p02/discussion/discussionpractice.txt

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git log   # to check all commits you have made
commit 188c9de6004b30b146b6bb392d27b81b43c244c9 (HEAD -> master)
Author: bihag.rudolf <bihag.rudolf@gmail.com>
Date:   Thu May 4 11:08:34 2023 +0800

    initial practice commit

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git status  #i made some changes to the discussionpractice.txt hence i checked and it was detected
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   p02/discussion/discussionpractice.txt

no changes added to commit (use "git add" and/or "git commit -a")

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git add .   #i added everything again

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git status   #just to check
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   p02/discussion/discussionpractice.txt


ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git status  #ignore repeat of above only
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   p02/discussion/discussionpractice.txt


ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git commit -m "add practice"  #to make new commit/log from the changes i created and named it "add practice"
[master 920db0f] add practice
 1 file changed, 3 insertions(+)

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git status  #to check if eveything was committed
On branch master
nothing to commit, working tree clean

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git add .   #unneccesary command

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git remote add origin git@gitlab.com:practiceb282/frontendpractice.git  #ssh from gitlab

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git remote -v   #check if remote repository is working
origin  git@gitlab.com:practiceb282/frontendpractice.git (fetch)
origin  git@gitlab.com:practiceb282/frontendpractice.git (push)

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git push origin master  #push files from local to remote repository
Enumerating objects: 10, done.
Counting objects: 100% (10/10), done.
Delta compression using up to 16 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (10/10), 654 bytes | 327.00 KiB/s, done.
Total 10 (delta 0), reused 0 (delta 0), pack-reused 0
remote:
remote: ========================================================================
remote:
remote:      The 16.0 major release is coming on May 22, 2023! This version
remote:    brings many exciting improvements to GitLab, but also removes some
remote:     deprecated features. These changes are going live on GitLab.com
remote:    now, continuing up to May 22, 2023, when GitLab 16.0 is released.
remote:                             Visit the <b><a
remote: href="https://docs.gitlab.com/ee/update/deprecations?removal_milestone=16.0&breaking_only=false">deprecations
remote:            page</a></b> to see what is scheduled for removal.
remote:
remote: ========================================================================
remote:
To gitlab.com:practiceb282/frontendpractice.git
 * [new branch]      master -> master

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git status  #check if you need to commit anything
On branch master
nothing to commit, working tree clean

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git status  #after making another change i checked and it was detected
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   p02/discussion/discussionpractice.txt

no changes added to commit (use "git add" and/or "git commit -a")


ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git add .   #add new changes

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git status
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   p02/discussion/discussionpractice.txt


ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git push origin master
Everything up-to-date

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git log
commit 920db0f31dc23f45eaa4bbc042a8dfed2f00a283 (HEAD -> master, origin/master)
Author: bihag.rudolf <bihag.rudolf@gmail.com>
Date:   Thu May 4 11:12:14 2023 +0800

    add practice

commit 188c9de6004b30b146b6bb392d27b81b43c244c9
Author: bihag.rudolf <bihag.rudolf@gmail.com>
Date:   Thu May 4 11:08:34 2023 +0800

    initial practice commit

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git add .

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git commit -m "final practice"
[master 7386b34] final practice
 1 file changed, 5 insertions(+), 1 deletion(-)

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git add .

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git status
On branch master
nothing to commit, working tree clean

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git push origin master
Enumerating objects: 9, done.
Counting objects: 100% (9/9), done.
Delta compression using up to 16 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (5/5), 389 bytes | 389.00 KiB/s, done.
Total 5 (delta 0), reused 0 (delta 0), pack-reused 0
remote:
remote: ========================================================================
remote:
remote:      The 16.0 major release is coming on May 22, 2023! This version
remote:    brings many exciting improvements to GitLab, but also removes some
remote:     deprecated features. These changes are going live on GitLab.com
remote:    now, continuing up to May 22, 2023, when GitLab 16.0 is released.
remote:                             Visit the <b><a
remote: href="https://docs.gitlab.com/ee/update/deprecations?removal_milestone=16.0&breaking_only=false">deprecations
remote:            page</a></b> to see what is scheduled for removal.
remote:
remote: ========================================================================
remote:
To gitlab.com:practiceb282/frontendpractice.git
   920db0f..7386b34  master -> master

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
$ git log
commit 7386b349168255fc947ab748bc6f5c9f737aea61 (HEAD -> master, origin/master)
Author: bihag.rudolf <bihag.rudolf@gmail.com>
Date:   Thu May 4 11:22:03 2023 +0800

    final practice

commit 920db0f31dc23f45eaa4bbc042a8dfed2f00a283
Author: bihag.rudolf <bihag.rudolf@gmail.com>
Date:   Thu May 4 11:12:14 2023 +0800

    add practice

commit 188c9de6004b30b146b6bb392d27b81b43c244c9
Author: bihag.rudolf <bihag.rudolf@gmail.com>
Date:   Thu May 4 11:08:34 2023 +0800

    initial practice commit

ASUS@DESKTOP-VB8V2GI MINGW64 ~/Documents/practice/frontend (master)
